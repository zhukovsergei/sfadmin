<?php


namespace App\Repository\User;


class UserView
{
    public $id;
    public $email;
    public $password;
    public $name;
    public $role;
    public $status;
}