<?php

namespace App\Controller\Admin\Widget;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class NavbarController extends AbstractController
{

    public function show(): Response
    {
        return $this->render('admin/widget/_navbar.html.twig');
    }
}
