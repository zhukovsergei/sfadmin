<?php

namespace App\Controller\Admin\Widget;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class FooterController extends AbstractController
{

    public function show(): Response
    {
        return $this->render('admin/widget/_footer.html.twig');
    }
}
