<?php

namespace App\Controller\Admin;


use App\Controller\Admin\Form\User\Create\Command;
use App\Controller\Admin\Form\User\Create\Form;
use App\Controller\Admin\Form\User\Create\Handler;
use App\Repository\User\UserFetcher;
use App\Service\User\PasswordHasher;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    private UserFetcher $users;

    public function __construct(UserFetcher $users)
    {
        $this->users = $users;
    }

    /**
     * @Route("/cp/user", name="admin.user.index")
     */
    public function index(): Response
    {
        $users = $this->users->findAll();

        return $this->render('admin/user/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/cp/user/create", name="admin.user.create")
     */
    public function create(Request $request, Handler $handler): Response
    {
        $command = new Command();
        $form = $this->createForm(Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $handler->handle($command);
        }

        return $this->render('admin/user/create.html.twig', [
            'form' => $form->createView()
        ]);
        /*
                $user = User::create('admin@admin.admin', $hasher->hash('admin'));
                $entityManager->persist($user);
                $entityManager->flush();*/
    }


}
