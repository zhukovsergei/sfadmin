<?php

namespace App\Controller\Admin;

use phpcent\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class SocketController extends AbstractController
{
    /**
     * @Route("/cp/socket", name="admin.socket.index")
     */
    public function index(Client $centrifugo): Response
    {
        $centrifugo->publish('alerts', ['message' => 'lol keke cheburek']);

        return new Response();
    }
}
