<?php

namespace App\Controller\Admin\Form\User\Create;

use App\Entity\User\Email;
use App\Entity\User\Name;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Repository\User\UserRepository;
use App\Service\Doctrine\Flusher;
use App\Service\User\PasswordGenerator;
use App\Service\User\PasswordHasher;

class Handler
{
    private $users;
    private $hasher;
    private $generator;
    private $flusher;

    public function __construct(
        UserRepository $users,
        PasswordHasher $hasher,
        PasswordGenerator $generator,
        Flusher $flusher
    )
    {
        $this->users = $users;
        $this->hasher = $hasher;
        $this->generator = $generator;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        $email = new Email($command->email);

        if ($this->users->hasByEmail($email)) {
            throw new \DomainException('User with this email already exists.');
        }

        $user = User::create(
            $email,
            $this->hasher->hash($command->password),
            new Name(
                $command->firstName,
                $command->lastName
            ),
            Role::user(),
            User::STATUS_ACTIVE
        );

        $this->users->add($user);

        $this->flusher->flush($user);
    }
}
