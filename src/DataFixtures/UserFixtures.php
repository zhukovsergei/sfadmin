<?php

namespace App\DataFixtures;

use App\Entity\User\Email;
use App\Entity\User\Name;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Service\User\PasswordHasher;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $admin = User::create(
            new Email('admin@admin.admin'),
            '$argon2i$v=19$m=65536,t=4,p=1$ZXBGYk5qWGw1V2ppLnFEOQ$mtiemsfZTjXkdo13cmO0O73INpJxQwDB4em3VyyQUe0',
            new Name('Vasya', 'Pupkin'),
            Role::admin(),
            User::STATUS_ACTIVE);

        $manager->persist($admin);

        $faker = Factory::create();
        $passwordHasher = new PasswordHasher();

        for ($i = 0; $i < 20; $i++) {
            $user = User::create(
                new Email($faker->email),
                $faker->password,
                new Name($faker->firstName, $faker->lastName),
                Role::user(),
                User::STATUS_ACTIVE
            );
            $manager->persist($user);
        }

        $manager->flush();
    }
}
