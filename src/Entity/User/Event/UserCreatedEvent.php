<?php

declare(strict_types=1);

namespace App\Entity\User\Event;


use App\Entity\User\User;

class UserCreatedEvent
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
