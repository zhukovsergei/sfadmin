<?php

declare(strict_types=1);

namespace App\Entity\User\Event;


class UserLoggedInEvent
{
    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }
}
