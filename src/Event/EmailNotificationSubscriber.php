<?php

declare(strict_types=1);

namespace App\Event;

use App\Entity\User\Event\UserCreatedEvent;
use App\Entity\User\Event\UserLoggedInEvent;

use App\Repository\User\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class EmailNotificationSubscriber implements EventSubscriberInterface
{
    private $tasks;
    private $members;
    private $mailer;
    private $twig;

    public function __construct( MailerInterface $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            UserLoggedInEvent::class => [
                ['onUserLoggedInEvent']
            ],
            UserCreatedEvent::class => [
                ['onUserCreatedEvent']
            ],
        ];
    }


    public function onUserLoggedInEvent(UserLoggedInEvent $event): void
    {
        dd($event);
    }

    public function onUserCreatedEvent(UserCreatedEvent $event): void
    {

        $message = (new Email())
            ->from('hello@example.com')
            ->to($event->user->getEmail())
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Time for Symfony Mailer!')
            ->text('Sending emails is fun again!')
            ->html($this->twig->render('mail/user/created.html.twig', [
                'user' => $event->user,
            ]), 'text/html');

        $this->mailer->send($message);

    }
}
