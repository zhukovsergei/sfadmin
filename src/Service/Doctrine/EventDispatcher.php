<?php

namespace App\Service\Doctrine;

interface EventDispatcher
{
    public function dispatch(array $events): void;
}
