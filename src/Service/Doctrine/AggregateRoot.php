<?php


namespace App\Service\Doctrine;

interface AggregateRoot
{
    public function releaseEvents(): array;
}
