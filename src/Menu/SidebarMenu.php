<?php

declare(strict_types=1);

namespace App\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class SidebarMenu
{
    private $factory;

    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function build(): ItemInterface
    {
        $menu = $this->factory->createItem('root')
            ->setChildrenAttributes(['class' => 'sidenav-inner py-1']);

        $menu->addChild('Dashboard', ['route' => 'admin.site.index'])
            ->setExtra('icon', 'sidenav-icon ion ion-md-desktop')
            ->setAttribute('class', 'sidenav-item')
            ->setLinkAttribute('class', 'sidenav-link');


        $menu->addChild('Users', ['route' => 'admin.user.index'])
            ->setExtra('icon', 'sidenav-icon ion ion-ios-contact')
            ->setAttribute('class', 'sidenav-item')
            ->setLinkAttribute('class', 'sidenav-link');


        return $menu;
    }
}
