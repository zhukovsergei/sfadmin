docker-build:
	docker-compose build

docker-stop:
	docker-compose stop

docker-up:
	docker-compose up -d

yarn-install:
	docker-compose run --rm manager-node yarn install

yarn-dev:
	docker-compose run --rm manager-node yarn encore dev --watch

composer-install:
	docker-compose run --rm php-cli composer install