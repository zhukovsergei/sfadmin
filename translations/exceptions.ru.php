<?php

declare(strict_types=1);


return [
    'Incorrect email.' => 'Неправильный email',
    'User with this email already exists.' => 'Пользователь с таким email уже существует',
    'User is not found.' => 'Пользователь не найден',
    'Username could not be found.' => 'Пользователь не найден',
    'User account is disabled.' => 'Аккаунт отключен',
];